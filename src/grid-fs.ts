import {ReadStream} from 'fs';
import Model from './model';
import Nongo from './nongo';

/**
 * Used to query fs.files collection and modify meta data. In order to actually store files in gridfs
 * use {@code nongo.gfs.write(..))
 *
 * @author Stuart
 */
export default class GridFs extends Model {

  constructor(nongo: Nongo, obj: any) {
    super(nongo, obj);
  }

  /**
   * @returns {module:fs.ReadStream} the read stream for the file associated with {@code this}.
   */
  public getReadStream(): ReadStream {
    return this.nongo.gfs.read(this.obj._id);
  }

  protected defineCollection() {
    return 'fs.files';
  }

  protected defineSchema(): any {
    return {

      filename: {
        type: 'string',
      },

      contentType: {
        type: 'string',
      },

      length: {
        type: 'number',
      },

      chunkSize: {
        type: 'number',
      },

      uploadDate: {
        type: 'date',
      },

      md5: {
        type: 'string',
      },

      metadata: {
        type: 'object',
      },

    };
  }

}
