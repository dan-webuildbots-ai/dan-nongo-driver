import * as deepEqual from 'deep-equal';
import {Db, MongoClient, ObjectId} from 'mongodb';

import GridFsController from './grid-fs-controller';
import { Newable } from './interfaces';
import Model from './model';
import MongoIndex from './mongo-index';
import MongoIndexSet from './mongo-index-set';
import NongoParams from './nongo-params';
import SchemaParser from './schema-parser';

export default class Nongo {

  /*
   * @return a new ObjectId if {@code id} is null, an ObjectId created using {@code id}.
   */
  public static toObjectId(id) {
    // Set the object id if there isn't one already
    if (id == null) {
      id = new ObjectId();

      // If {@code this.obj._id} is not an {@code ObjectId} turn it into one
    } else if (!(id instanceof ObjectId)) {
      id = new ObjectId(id);
    }

    return id;
  }

  public new: any = {};
  public schema: any = {};
  public validKeys: any = {};
  public noStripKeys: any = {};
  public db: Db;
  public dbName: string;

  public noIndex: boolean;
  public gfs: GridFsController;

  // {@code modelClasses} should actually be of type {@code Model[]} throws a
  // compiler error when calling {@code Class.name}
  constructor(private params: NongoParams, private modelClasses: Newable[], public schemaParsers?: SchemaParser[]) {

  }

  public async connect() {
    // Connect to the database
    this.db = (await MongoClient.connect(this.uri())).db(this.params.db);
    this.dbName = (this.db as any).s.databaseName;
    this.gfs = new GridFsController(this);

    // Map the classes to instances
    const instances = this.modelClasses.map((Clazz) => new Clazz(this, null));

    // Init the schema parsers if they are aren't already given
    if (!this.schemaParsers) {
      this.schemaParsers = instances.map((instance) => {
        const collectionName = instance.name;
        // Validate the schema (throws error if not valid)
        return new SchemaParser(instance.defineSchema(), collectionName);
      });
    }

    // Iterate over each of the model classes
    for (let i = 0; i < instances.length; i++) {
      const Clazz = this.modelClasses[i];
      const instance = instances[i];
      const parser = this.schemaParsers[i];
      const schema = parser.schema;

      // Create a field on {@code this} that can be used to access what appear to
      // client code to be static methods of the model e.g. {@code nongo.MyModel.find(..)}
      this[instance.name] = instance;

      // Create a field on {@code this.validKeys} for the model
      this.validKeys[instance.name] = parser.validKeys;
      this.noStripKeys[instance.name] = parser.noStripKeys;

      // Create a field on {@code this.schema} for the model
      this.schema[instance.name] = schema;

      // Ensure all the indexes defined in the schema
      if (!this.noIndex) {
        await this.ensureIndexes(schema, instance.collection);
      }

      // Add field to {@code this.new} with a function as a value that can be
      // used to init a new model
      this.new[instance.name] = (obj) => {
        return new Clazz(this, obj);
      };

    }

    return this;
  }

  public async dropDatabase() {
    await this.db.dropDatabase();
  }

  public async collectionExisits(collName: string) {
    const collections = await this.db.listCollections().toArray();
    return collections.some((col) => col.name === collName);
  }

  /**
   * @returns {string} the mongo uri built from {@code this.params}.
   */
  public uri() {
    // Protocol
    let url = (this.params.protocol || 'mongodb') + '://';

    // Authentication (optional)
    if (this.params.username) {
      url += this.params.username + ':' + this.params.password + '@';
    }

    // Host
    url += this.params.host;
    // Then optionally the port

    // Port (optional)
    if (this.params.port) {
      url += ':' + this.params.port;
    }

    // Database
    url += '/' + this.params.db + '?';

    // Options given as uri params
    const options = this.params.options || {};
    url += Object.keys(options).map((key) => key + '=' + encodeURI(options[key])).join('&');

    return url;
  }

  /**
   * Helper function to retrieve a model in a type-safe manner
   * @param modelName
   */
  public getModel<U extends Model>(modelName: string): U {
    return this[modelName];
  }

  /**
   * Helper function to create a new model using the supplied object
   * @param collectionName
   * @param obj
   */
  public newModel<U extends Model>(collectionName: string, obj: U['obj']): U {
    return this.new[collectionName](obj);
  }

  private async ensureIndexes(schema: any, collName: string) {
    // Get the existing set of indexes, we will remove the ones we want to keep
    const toDrop = await this.existingIndexes(collName);

    // Ensure all the required indexes have been applied
    const indexSet = await this.getSchemaIndexes(schema);

    const collection = this.db.collection(collName);
    for (const index of indexSet) {

      // If there is an existing index with the same name as {@code index} but different options
      // drop the existing index
      const existingIndex = toDrop.get(index);
      if (existingIndex && !deepEqual(index.options, existingIndex.options)) {
        await collection.dropIndex(existingIndex.name);
      }

      // Ensure the index then remove it from the {@code toDrop} set
      await (collection as any).ensureIndex(index.index, index.options);
      toDrop.remove(index);
    }

    // Remove any indexes that are no longer required
    for (const index of toDrop.values()) {
      await collection.dropIndex(index.name);
    }
  }

  private async existingIndexes(collName: string) {
    const indexSet = new MongoIndexSet();

    // If the collection exists
    if (await this.collectionExisits(collName)) {

      // Iterate over the existing indexes
      const collection = this.db.collection(collName);
      for (const indexInfo of await collection.indexes()) {

        // Parse the index info
        const path = Object.keys(indexInfo.key)[0];
        const order = Object.values(indexInfo.key)[0] as number;
        const options = {
          unique: indexInfo.unique,
        };

        // Add all the indexes to the {@code indexSet} except _id
        if (path !== '_id') {
          indexSet.add(new MongoIndex(path, options, order));
        }

      }

    }

    return indexSet;
  }

  /**
   * Recursively traverse the schema to populate the {@code indexList} with {@link Index}s constructed using schema
   * kv-pairs.
   *
   * @param parentSchema
   * @param {string} parentPath
   * @param {Map<string, Index>} indexMap
   * @returns {Promise<Map<string, Index>>}
   */
  private async getSchemaIndexes(parentSchema: any, parentPath = '', indexList = []): Promise<MongoIndex[]> {

    for (const key of Object.keys(parentSchema)) {
      // Define the path to the current key
      const path = parentPath ? parentPath + '.' + key : key;

      // Ensure an index if there should be one
      const schema = parentSchema[key];
      if (schema.unique) {
        indexList.push(new MongoIndex(path, {unique: true}));
      } else if (schema.indexed) {
        indexList.push(new MongoIndex(path));
      }

      // Make a recursive call if required
      const type = schema.type;
      if (Array.isArray(type)) {
        const elType = type[0];
        if (typeof elType === 'object') {
          await this.getSchemaIndexes(elType, path, indexList);
        }
      } else if (typeof type === 'object') {
        await this.getSchemaIndexes(type, path, indexList);
      }

    }

    return indexList;
  }

}
