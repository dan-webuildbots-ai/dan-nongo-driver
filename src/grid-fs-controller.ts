import {ReadStream} from 'fs';
import * as Grid from 'gridfs-stream';
import * as mongo from 'mongodb';
import GridFs from './grid-fs';

/**
 * Used to store files in mongodb.
 *
 * @author Stuart
 */
export default class GridFsController {

  private gfs;

  constructor(private nongo) {
    this.gfs = Grid(nongo.db, mongo);
  }

  /**
   * Used to write a fileStream to gridfs.
   *
   * @param fileStream - e.g. {@code fs.createReadStream('/some/path')}
   * @param options - see {@link https://github.com/aheckmann/gridfs-stream#createwritestream}.
   */
  public write(fileStream: ReadStream, options): Promise<GridFs> {
    return new Promise((resolve, reject) => {
      // streaming to gridfs
      const writeStream = this.gfs.createWriteStream(options);
      fileStream.pipe(writeStream);

      writeStream.on('close', (obj) => {
        resolve(this.nongo.new.GridFs(obj));
      });

      writeStream.on('error', (err) => {
        reject(err);
      });
    });
  }

  /**
   * Used to read in a file from fs.chunks.
   *
   * @param id the {@code _id} of the fs.files / {@link GridFs} object.
   * @returns {module:fs.ReadStream}
   */
  public read(id: any): ReadStream {
    if (id instanceof mongo.ObjectId) {
      id = id.toHexString();
    }
    return this.gfs.createReadStream({_id: id});
  }

}
