/**
 * Very basic model used to hold information on a mongo index.
 */
export default class MongoIndex {

  public readonly index: any;
  public readonly name: string;

  /**
   * @param {string} path - the path to the field that should be indexed.
   * @param {{}} options - options for the index these match the options defined here:
   *                       https://mongodb.github.io/node-mongodb-native/api-generated/collection.html#ensureindex
   * @param {number} order - the order that the index should be created ascending (1) or descending (-1).
   */
  constructor(path: string, public readonly options = {}, order = 1) {
    // Check the order value is correct
    if (order !== -1 && order !== 1) {
      throw Error('order must have the value 1 or -1');
    }

    // Define the index
    this.index = {};
    this.index[path] = order;
    this.name = `${path}_${order}`;
  }

}
