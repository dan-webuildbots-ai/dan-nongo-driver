import {createLogger, format, transports} from 'winston';

const logger: any = createLogger({
  format: format.combine(
    format.timestamp(),
    format.splat(),
    format.simple(),
  ),
  transports: [new transports.Console()],
});

export default logger;
