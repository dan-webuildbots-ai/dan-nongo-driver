"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Collection used to maintain a set of {@link MongoIndex}s. {@link MongoIndex}s are concidered the same if they have
 * a matching {@code name}.
 */
class MongoIndexSet {
    constructor() {
        this.indexMap = new Map();
    }
    add(mIndex) {
        this.indexMap.set(mIndex.name, mIndex);
    }
    get(mIndex) {
        return this.indexMap.get(mIndex.name);
    }
    has(mIndex) {
        this.indexMap.has(mIndex.name);
    }
    values() {
        return this.indexMap.values();
    }
    remove(mIndex) {
        return this.indexMap.delete(mIndex.name);
    }
}
exports.default = MongoIndexSet;
