"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nongo_1 = require("./nongo");
/**
 * Used to efficiently set up and maintain multiple nongo connections that use the same models
 */
class NongoMulti {
    constructor(paramsArray, modelClasses) {
        this.paramsArray = paramsArray;
        this.modelClasses = modelClasses;
        this.nongoMap = {};
    }
    async connect() {
        // Make the first nongo connection
        const firstParams = this.paramsArray[0];
        this.checkHasId(firstParams);
        const firstNongo = await new nongo_1.default(firstParams, this.modelClasses).connect();
        this.nongoMap[firstParams.id] = firstNongo;
        // Connect all the other instances using the schemaParsers from  {@code firstNongo}
        for (let i = 1; i < this.paramsArray.length; i++) {
            const params = this.paramsArray[i];
            this.nongoMap[params.id] = await new nongo_1.default(params, this.modelClasses, firstNongo.schemaParsers).connect();
        }
        return this;
    }
    getNongo(id) {
        return this.nongoMap[id];
    }
    checkHasId(params) {
        if (params.id == null) {
            throw Error('One the NongoParams objects given is missing an id field');
        }
    }
}
exports.default = NongoMulti;
