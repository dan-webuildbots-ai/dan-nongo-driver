import { Db } from 'mongodb';
import GridFsController from './grid-fs-controller';
import { Newable } from './interfaces';
import Model from './model';
import NongoParams from './nongo-params';
import SchemaParser from './schema-parser';
export default class Nongo {
    private params;
    private modelClasses;
    schemaParsers?: SchemaParser[];
    static toObjectId(id: any): any;
    new: any;
    schema: any;
    validKeys: any;
    noStripKeys: any;
    db: Db;
    dbName: string;
    noIndex: boolean;
    gfs: GridFsController;
    constructor(params: NongoParams, modelClasses: Newable[], schemaParsers?: SchemaParser[]);
    connect(): Promise<this>;
    dropDatabase(): Promise<void>;
    collectionExisits(collName: string): Promise<boolean>;
    /**
     * @returns {string} the mongo uri built from {@code this.params}.
     */
    uri(): string;
    /**
     * Helper function to retrieve a model in a type-safe manner
     * @param modelName
     */
    getModel<U extends Model>(modelName: string): U;
    /**
     * Helper function to create a new model using the supplied object
     * @param collectionName
     * @param obj
     */
    newModel<U extends Model>(collectionName: string, obj: U['obj']): U;
    private ensureIndexes;
    private existingIndexes;
    /**
     * Recursively traverse the schema to populate the {@code indexList} with {@link Index}s constructed using schema
     * kv-pairs.
     *
     * @param parentSchema
     * @param {string} parentPath
     * @param {Map<string, Index>} indexMap
     * @returns {Promise<Map<string, Index>>}
     */
    private getSchemaIndexes;
}
