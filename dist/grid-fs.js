"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const model_1 = require("./model");
/**
 * Used to query fs.files collection and modify meta data. In order to actually store files in gridfs
 * use {@code nongo.gfs.write(..))
 *
 * @author Stuart
 */
class GridFs extends model_1.default {
    constructor(nongo, obj) {
        super(nongo, obj);
    }
    /**
     * @returns {module:fs.ReadStream} the read stream for the file associated with {@code this}.
     */
    getReadStream() {
        return this.nongo.gfs.read(this.obj._id);
    }
    defineCollection() {
        return 'fs.files';
    }
    defineSchema() {
        return {
            filename: {
                type: 'string',
            },
            contentType: {
                type: 'string',
            },
            length: {
                type: 'number',
            },
            chunkSize: {
                type: 'number',
            },
            uploadDate: {
                type: 'date',
            },
            md5: {
                type: 'string',
            },
            metadata: {
                type: 'object',
            },
        };
    }
}
exports.default = GridFs;
