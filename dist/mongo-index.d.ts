/**
 * Very basic model used to hold information on a mongo index.
 */
export default class MongoIndex {
    readonly options: {};
    readonly index: any;
    readonly name: string;
    /**
     * @param {string} path - the path to the field that should be indexed.
     * @param {{}} options - options for the index these match the options defined here:
     *                       https://mongodb.github.io/node-mongodb-native/api-generated/collection.html#ensureindex
     * @param {number} order - the order that the index should be created ascending (1) or descending (-1).
     */
    constructor(path: string, options?: {}, order?: number);
}
