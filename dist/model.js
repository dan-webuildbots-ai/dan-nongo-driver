"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const clone = require("clone-deep");
const cmd = require("cmd-promise");
const logger_1 = require("./logger");
const nongo_1 = require("./nongo");
const validator_1 = require("./validator");
class Model {
    constructor(nongo, obj) {
        this.nongo = nongo;
        this.obj = obj;
        // Set this.db if nongo given
        if (nongo) {
            this.db = nongo.db;
        }
        this.collection = this.defineCollection();
        this.name = this.constructor.name;
        this.dynamic = false;
        this.initStructure();
    }
    async validate() {
        const schema = this.nongo.schema[this.name];
        return await new validator_1.default(schema, this).validate();
    }
    async dumpCollection(dir, gzip = false) {
        logger_1.default.info(`Taking dump of ${this.name}...`);
        const script = `mkdir -p ${dir};` +
            `mongodump --uri "${this.nongo.uri()}" -c ${this.name} ${gzip ? '--gzip' : ''} --out ${dir};`;
        await cmd(script);
        logger_1.default.info(`Dump written to ${dir}...`);
    }
    /*
     * Upsert the model into the database
     */
    async save() {
        // Set the object id if there isn't one already
        this.obj._id = nongo_1.default.toObjectId(this.obj._id);
        // Make a deep clone so that the original object does not have it's fields striped
        this.obj = clone(this.obj);
        // Strip all the keys from the object that are not present in the schema
        // if not a dynamic model
        if (!this.dynamic) {
            this.strip('', this.obj);
        }
        // Validate the object
        const err = await this.validate();
        if (err) {
            throw Error(JSON.stringify(err) + ' failed to save the following obj:\n' + JSON.stringify(this.obj));
        }
        // Upsert
        await this.db.collection(this.collection)
            .replaceOne({ _id: this.obj._id }, this.obj, { upsert: true });
        // Return the saved model
        return this;
    }
    /*
     * Recursive method used to strip fields from an object have not been defined
     * by the schema.
     */
    strip(parentPath, toStrip) {
        for (const key of Object.keys(toStrip)) {
            const path = parentPath ? parentPath + '.' + key : key;
            // Skip over keys which shouldn't be stripped
            if (this.nongo.noStripKeys[this.constructor.name].has(path)) {
                continue;
            }
            // If the key is not in the schema delete it
            if (!this.nongo.validKeys[this.constructor.name].has(path)) {
                this.deleteKey(path, this.obj);
                // If the key is in the schema examin it's children if there are any
            }
            else {
                const value = toStrip[key];
                // If value is an array call strip on all of it's elements that are objects
                if (Array.isArray(value)) {
                    value.forEach((element) => {
                        if (typeof element === 'object') {
                            this.strip(path, element);
                        }
                    });
                    // If the value is a nested object call strip on it
                }
                else if (value != null && typeof value === 'object') {
                    this.strip(path, value);
                }
            }
        }
    }
    /*
     * Recursive method used to delete keys from models using a schema path.
     * This includeds the handling of arrays
     *
     * @param the path to the key which should be deleted.
     * @param the object to which the path is relative.
     */
    deleteKey(path, obj) {
        // If there is no . remaining in the path then delete the required key
        const indexFirstDot = path.indexOf('.');
        if (indexFirstDot === -1) {
            delete obj[path];
        }
        else {
            // Make a recusive call dependant on the value type
            const value = obj[path.slice(0, indexFirstDot)];
            const pathRemaining = path.slice(indexFirstDot + 1);
            if (Array.isArray(value)) {
                value.forEach((el) => this.deleteKey(pathRemaining, el));
            }
            else {
                this.deleteKey(pathRemaining, value);
            }
        }
    }
    async byId(id) {
        return await this.findOne({ _id: id });
    }
    /*
     * @param query: A mongo db query.
     * @param options: the query options e.g. sorting, limiting results etc.
     * @return the results of the query as an array of models.
     */
    // @ts-ignore
    async find(query = {}, options) {
        query = this.prepareQuery(query);
        const docs = await this.db.collection(this.collection)
            .find(query, options).toArray();
        // Map the docs to the correct model
        // @ts-ignore
        return docs.map((obj) => new this.constructor(this.nongo, obj));
    }
    /*
     * @param query: A mongo db query.
     * @return the result of the query as a model.
     */
    // @ts-ignore
    async findOne(query = {}) {
        query = this.prepareQuery(query);
        const doc = await this.db.collection(this.collection).findOne(query);
        // Return null if no doc was found
        if (doc == null) {
            return null;
        }
        // Otherwise return the doc as a model
        // @ts-ignore
        return new this.constructor(this.nongo, doc);
    }
    async remove(query) {
        query = this.prepareQuery(query);
        const res = await this.db.collection(this.collection).remove(query);
        return res.result;
    }
    async aggregate(pipeline, options) {
        return await this.db.collection(this.collection)
            .aggregate(pipeline, options).toArray();
    }
    async distinct(field) {
        return await this.db.collection(this.collection).distinct(field, {});
    }
    async update(query, updates) {
        query = this.prepareQuery(query);
        const options = {
            multi: true,
        };
        const res = await this.db.collection(this.collection).update(query, updates, options);
        return res.result;
    }
    async count(query = {}) {
        return await this.db.collection(this.collection).count(query);
    }
    /*
     * This method should be overridden if want to use something other than the
     * class name as the collection name.
     */
    defineCollection() {
        return this.constructor.name;
    }
    /*
     * Must be run on all queries before they are used.
     */
    prepareQuery(query) {
        // Make sure that _id is an ObjectId if it is set
        if (query._id && typeof query._id === 'string') {
            query._id = nongo_1.default.toObjectId(query._id);
        }
        return query;
    }
    /*
     * Initialises any objects and arrays which are marked as required in the schema.
     */
    initStructure() {
        // Init this.obj if it is null
        if (this.obj == null) {
            this.obj = {};
        }
        // We get the schema from nongo not this.getSchema() for the sake of efficiency
        if (this.nongo && this.nongo.schema[this.name]) {
            this.initKey(this.obj, this.nongo.schema[this.name]);
        }
    }
    initKey(parent, parentSchema) {
        for (const key of Object.keys(parentSchema)) {
            const schema = parentSchema[key];
            const type = schema.type;
            // Evaluate schema.required to a boolean
            let required = Array.isArray(schema.required) ? schema.required[0] : schema.required;
            required = typeof required === 'function' ? required(parent[key], this.obj) : required;
            const isNullRequired = parent[key] == null && required;
            if (Array.isArray(type)) {
                if (isNullRequired) {
                    parent[key] = [];
                }
                // Make recursive call if not null and an array and child type is not native type
                if (parent[key] != null && Array.isArray(parent[key])) {
                    const childType = type[0];
                    if (typeof childType === 'object') {
                        parent[key].forEach((el) => this.initKey(el, childType));
                    }
                }
            }
            else if (typeof type === 'object') {
                // Init the the object if null and required
                if (isNullRequired) {
                    parent[key] = {};
                }
                // Make recusive call if not null and an object
                if (parent[key] != null && typeof parent[key] === 'object') {
                    this.initKey(parent[key], type);
                }
            }
            else if (type === 'object' && isNullRequired) {
                parent[key] = {};
            }
        }
    }
}
exports.default = Model;
