import { Db, ObjectId } from 'mongodb';
import { NongoDeleteResult, NongoWriteResult } from './interfaces';
import Nongo from './nongo';
export default abstract class Model<T extends {
    _id?: ObjectId;
} = any> {
    nongo: Nongo;
    obj: T;
    collection: string;
    db: Db;
    name: string;
    dynamic: boolean;
    constructor(nongo: Nongo, obj: T);
    validate(): Promise<string[]>;
    dumpCollection(dir: string, gzip?: boolean): Promise<void>;
    save(): Promise<this>;
    strip(parentPath: any, toStrip: any): void;
    deleteKey(path: any, obj: any): void;
    byId(id: any): Promise<this>;
    find(query?: any, options?: any): Promise<this[]>;
    findOne(query?: any): Promise<this>;
    remove(query: any): Promise<NongoDeleteResult>;
    aggregate(pipeline: any, options?: any): Promise<any[]>;
    distinct(field: string): Promise<any>;
    update(query: any, updates: any): Promise<NongoWriteResult>;
    count(query?: {}): Promise<number>;
    protected defineCollection(): string;
    protected abstract defineSchema(): any;
    private prepareQuery;
    private initStructure;
    private initKey;
}
