"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Grid = require("gridfs-stream");
const mongo = require("mongodb");
/**
 * Used to store files in mongodb.
 *
 * @author Stuart
 */
class GridFsController {
    constructor(nongo) {
        this.nongo = nongo;
        this.gfs = Grid(nongo.db, mongo);
    }
    /**
     * Used to write a fileStream to gridfs.
     *
     * @param fileStream - e.g. {@code fs.createReadStream('/some/path')}
     * @param options - see {@link https://github.com/aheckmann/gridfs-stream#createwritestream}.
     */
    write(fileStream, options) {
        return new Promise((resolve, reject) => {
            // streaming to gridfs
            const writeStream = this.gfs.createWriteStream(options);
            fileStream.pipe(writeStream);
            writeStream.on('close', (obj) => {
                resolve(this.nongo.new.GridFs(obj));
            });
            writeStream.on('error', (err) => {
                reject(err);
            });
        });
    }
    /**
     * Used to read in a file from fs.chunks.
     *
     * @param id the {@code _id} of the fs.files / {@link GridFs} object.
     * @returns {module:fs.ReadStream}
     */
    read(id) {
        if (id instanceof mongo.ObjectId) {
            id = id.toHexString();
        }
        return this.gfs.createReadStream({ _id: id });
    }
}
exports.default = GridFsController;
