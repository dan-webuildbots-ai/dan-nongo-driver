export default class Validator {
    private schema;
    private model;
    errs: string[];
    private obj;
    constructor(schema: any, model: any);
    validate(): Promise<string[]>;
    doValidate(parentPath: string, parentSchema: any, parent: any): Promise<void>;
    validateUnique(schema: any, value: any, path: any): Promise<boolean>;
    validateValue(schema: any, value: any, path: any): boolean;
    validateNotEmpty(schema: any, value: any, path: any): boolean;
    valueIsSet(schema: any, value: any, path: any): boolean;
    primitiveType(schema: any, value: any, path: any): boolean;
    correctPrimitive(type: string, value: any): boolean;
    primitiveArrayType(type: any, array: any, path: any): boolean;
    private processSchemaField;
    private addError;
}
