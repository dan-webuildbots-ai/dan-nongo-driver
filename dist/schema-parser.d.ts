export default class SchemaParser {
    schema: any;
    static standardSchemaError(path: string, fieldName: string): string;
    static notEmptyTypeError(path: string): string;
    validKeys: Set<string>;
    noStripKeys: Set<string>;
    constructor(schema: any, collection: string);
    parseSchema(parentPath: string, definition: any): void;
    checkIndexed(path: string, indexed: any): void;
    checkNotEmpty(path: string, notEmpty: any, type: string): void;
    checkValidate(path: string, validate: any): void;
    checkType(path: string, type: any): void;
    private validateStandardSchema;
    private validateArray;
}
