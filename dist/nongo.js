"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const deepEqual = require("deep-equal");
const mongodb_1 = require("mongodb");
const grid_fs_controller_1 = require("./grid-fs-controller");
const mongo_index_1 = require("./mongo-index");
const mongo_index_set_1 = require("./mongo-index-set");
const schema_parser_1 = require("./schema-parser");
class Nongo {
    // {@code modelClasses} should actually be of type {@code Model[]} throws a
    // compiler error when calling {@code Class.name}
    constructor(params, modelClasses, schemaParsers) {
        this.params = params;
        this.modelClasses = modelClasses;
        this.schemaParsers = schemaParsers;
        this.new = {};
        this.schema = {};
        this.validKeys = {};
        this.noStripKeys = {};
    }
    /*
     * @return a new ObjectId if {@code id} is null, an ObjectId created using {@code id}.
     */
    static toObjectId(id) {
        // Set the object id if there isn't one already
        if (id == null) {
            id = new mongodb_1.ObjectId();
            // If {@code this.obj._id} is not an {@code ObjectId} turn it into one
        }
        else if (!(id instanceof mongodb_1.ObjectId)) {
            id = new mongodb_1.ObjectId(id);
        }
        return id;
    }
    async connect() {
        // Connect to the database
        this.db = (await mongodb_1.MongoClient.connect(this.uri())).db(this.params.db);
        this.dbName = this.db.s.databaseName;
        this.gfs = new grid_fs_controller_1.default(this);
        // Map the classes to instances
        const instances = this.modelClasses.map((Clazz) => new Clazz(this, null));
        // Init the schema parsers if they are aren't already given
        if (!this.schemaParsers) {
            this.schemaParsers = instances.map((instance) => {
                const collectionName = instance.name;
                // Validate the schema (throws error if not valid)
                return new schema_parser_1.default(instance.defineSchema(), collectionName);
            });
        }
        // Iterate over each of the model classes
        for (let i = 0; i < instances.length; i++) {
            const Clazz = this.modelClasses[i];
            const instance = instances[i];
            const parser = this.schemaParsers[i];
            const schema = parser.schema;
            // Create a field on {@code this} that can be used to access what appear to
            // client code to be static methods of the model e.g. {@code nongo.MyModel.find(..)}
            this[instance.name] = instance;
            // Create a field on {@code this.validKeys} for the model
            this.validKeys[instance.name] = parser.validKeys;
            this.noStripKeys[instance.name] = parser.noStripKeys;
            // Create a field on {@code this.schema} for the model
            this.schema[instance.name] = schema;
            // Ensure all the indexes defined in the schema
            if (!this.noIndex) {
                await this.ensureIndexes(schema, instance.collection);
            }
            // Add field to {@code this.new} with a function as a value that can be
            // used to init a new model
            this.new[instance.name] = (obj) => {
                return new Clazz(this, obj);
            };
        }
        return this;
    }
    async dropDatabase() {
        await this.db.dropDatabase();
    }
    async collectionExisits(collName) {
        const collections = await this.db.listCollections().toArray();
        return collections.some((col) => col.name === collName);
    }
    /**
     * @returns {string} the mongo uri built from {@code this.params}.
     */
    uri() {
        // Protocol
        let url = (this.params.protocol || 'mongodb') + '://';
        // Authentication (optional)
        if (this.params.username) {
            url += this.params.username + ':' + this.params.password + '@';
        }
        // Host
        url += this.params.host;
        // Then optionally the port
        // Port (optional)
        if (this.params.port) {
            url += ':' + this.params.port;
        }
        // Database
        url += '/' + this.params.db + '?';
        // Options given as uri params
        const options = this.params.options || {};
        url += Object.keys(options).map((key) => key + '=' + encodeURI(options[key])).join('&');
        return url;
    }
    /**
     * Helper function to retrieve a model in a type-safe manner
     * @param modelName
     */
    getModel(modelName) {
        return this[modelName];
    }
    /**
     * Helper function to create a new model using the supplied object
     * @param collectionName
     * @param obj
     */
    newModel(collectionName, obj) {
        return this.new[collectionName](obj);
    }
    async ensureIndexes(schema, collName) {
        // Get the existing set of indexes, we will remove the ones we want to keep
        const toDrop = await this.existingIndexes(collName);
        // Ensure all the required indexes have been applied
        const indexSet = await this.getSchemaIndexes(schema);
        const collection = this.db.collection(collName);
        for (const index of indexSet) {
            // If there is an existing index with the same name as {@code index} but different options
            // drop the existing index
            const existingIndex = toDrop.get(index);
            if (existingIndex && !deepEqual(index.options, existingIndex.options)) {
                await collection.dropIndex(existingIndex.name);
            }
            // Ensure the index then remove it from the {@code toDrop} set
            await collection.ensureIndex(index.index, index.options);
            toDrop.remove(index);
        }
        // Remove any indexes that are no longer required
        for (const index of toDrop.values()) {
            await collection.dropIndex(index.name);
        }
    }
    async existingIndexes(collName) {
        const indexSet = new mongo_index_set_1.default();
        // If the collection exists
        if (await this.collectionExisits(collName)) {
            // Iterate over the existing indexes
            const collection = this.db.collection(collName);
            for (const indexInfo of await collection.indexes()) {
                // Parse the index info
                const path = Object.keys(indexInfo.key)[0];
                const order = Object.values(indexInfo.key)[0];
                const options = {
                    unique: indexInfo.unique,
                };
                // Add all the indexes to the {@code indexSet} except _id
                if (path !== '_id') {
                    indexSet.add(new mongo_index_1.default(path, options, order));
                }
            }
        }
        return indexSet;
    }
    /**
     * Recursively traverse the schema to populate the {@code indexList} with {@link Index}s constructed using schema
     * kv-pairs.
     *
     * @param parentSchema
     * @param {string} parentPath
     * @param {Map<string, Index>} indexMap
     * @returns {Promise<Map<string, Index>>}
     */
    async getSchemaIndexes(parentSchema, parentPath = '', indexList = []) {
        for (const key of Object.keys(parentSchema)) {
            // Define the path to the current key
            const path = parentPath ? parentPath + '.' + key : key;
            // Ensure an index if there should be one
            const schema = parentSchema[key];
            if (schema.unique) {
                indexList.push(new mongo_index_1.default(path, { unique: true }));
            }
            else if (schema.indexed) {
                indexList.push(new mongo_index_1.default(path));
            }
            // Make a recursive call if required
            const type = schema.type;
            if (Array.isArray(type)) {
                const elType = type[0];
                if (typeof elType === 'object') {
                    await this.getSchemaIndexes(elType, path, indexList);
                }
            }
            else if (typeof type === 'object') {
                await this.getSchemaIndexes(type, path, indexList);
            }
        }
        return indexList;
    }
}
exports.default = Nongo;
