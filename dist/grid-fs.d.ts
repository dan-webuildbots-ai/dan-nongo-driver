/// <reference types="node" />
import { ReadStream } from 'fs';
import Model from './model';
import Nongo from './nongo';
/**
 * Used to query fs.files collection and modify meta data. In order to actually store files in gridfs
 * use {@code nongo.gfs.write(..))
 *
 * @author Stuart
 */
export default class GridFs extends Model {
    constructor(nongo: Nongo, obj: any);
    /**
     * @returns {module:fs.ReadStream} the read stream for the file associated with {@code this}.
     */
    getReadStream(): ReadStream;
    protected defineCollection(): string;
    protected defineSchema(): any;
}
