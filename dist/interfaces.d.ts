/**
 * Defines an object that can be created via the 'new' keyword
 */
export interface Newable<T = any> {
    new (...args: any[]): T;
}
export interface NongoWriteResult {
    nModified: number;
    ok: 0 | 1;
    n: number;
}
export interface NongoDeleteResult {
    ok: 0 | 1;
    n: number;
}
