/// <reference types="node" />
import { ReadStream } from 'fs';
import GridFs from './grid-fs';
/**
 * Used to store files in mongodb.
 *
 * @author Stuart
 */
export default class GridFsController {
    private nongo;
    private gfs;
    constructor(nongo: any);
    /**
     * Used to write a fileStream to gridfs.
     *
     * @param fileStream - e.g. {@code fs.createReadStream('/some/path')}
     * @param options - see {@link https://github.com/aheckmann/gridfs-stream#createwritestream}.
     */
    write(fileStream: ReadStream, options: any): Promise<GridFs>;
    /**
     * Used to read in a file from fs.chunks.
     *
     * @param id the {@code _id} of the fs.files / {@link GridFs} object.
     * @returns {module:fs.ReadStream}
     */
    read(id: any): ReadStream;
}
