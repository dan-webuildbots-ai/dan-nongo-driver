"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("./logger");
const validSchemaKeys = new Set([
    'type',
    'required',
    'validate',
    'indexed',
    'unique',
    'notEmpty',
]);
class SchemaParser {
    constructor(schema, collection) {
        this.schema = schema;
        this.validKeys = new Set(['_id']);
        this.noStripKeys = new Set(['_id']);
        try {
            this.parseSchema('', schema);
        }
        catch (err) {
            logger_1.default.error('Failed to parse schema for collection named %s', collection);
            throw err;
        }
    }
    static standardSchemaError(path, fieldName) {
        const fullPath = path + '.' + fieldName;
        return 'If set, ' + fullPath + ' must be a boolean, a function that returns a boolean or an array of length 2.'
            + ' The 1st of the array elements can either be a boolean or a function that returns a boolean, if true the '
            + fullPath + ' field is active. The second element is either a string error'
            + ' message or a function that should return the error message.';
    }
    static notEmptyTypeError(path) {
        return path + '.type must be either "string" or "array" to be used with "notEmpty"';
    }
    parseSchema(parentPath, definition) {
        for (const key of Object.keys(definition)) {
            // Define the path to the current key
            const path = parentPath ? parentPath + '.' + key : key;
            // Add the path to the set of valid keys for the schema
            this.validKeys.add(path);
            // Get the definition for the child
            const subDef = definition[key];
            // Make sure that there are no keys on the schema which shouldn't be
            for (const subKey of Object.keys(subDef)) {
                if (!validSchemaKeys.has(subKey)) {
                    throw new Error(path + '.' + subKey + ' is not a vaid schema key, should ' +
                        subKey + ' be within a "type" object / the root object?');
                }
            }
            // Indexed
            this.checkIndexed(path, subDef.indexed);
            // Unique
            this.validateStandardSchema(subDef.unique, 'unique', path);
            // Required
            this.validateStandardSchema(subDef.required, 'required', path);
            // Validate
            this.validateStandardSchema(subDef.validate, 'validate', path);
            // This method makes a recursive call to validateSchema(..)
            this.checkType(path, subDef.type);
            // Not Empty (called after this.checkType as should only be used with strings and arrays)
            this.checkNotEmpty(path, subDef.notEmpty, subDef.type);
        }
    }
    checkIndexed(path, indexed) {
        const type = typeof indexed;
        const unsetOrBoolean = type === 'undefined' || type === 'boolean';
        if (!unsetOrBoolean) {
            throw new Error('If set, ' + path + '.indexed must be a boolean');
        }
    }
    checkNotEmpty(path, notEmpty, type) {
        this.validateStandardSchema(notEmpty, 'notEmpty', path);
        if (typeof notEmpty !== 'undefined' && !Array.isArray(type) && type !== 'string') {
            throw new Error(SchemaParser.notEmptyTypeError(path));
        }
    }
    checkValidate(path, validate) {
        if (validate) {
            // Is array of length 2 where both elements are functions
            const isValid = this.validateArray(validate, 2, ['function'], ['function']);
            if (!isValid) {
                throw new Error('If set ' + path + '.validate must be an array of length 2'
                    + ' where the 1st element is a function that returns true if'
                    + ' the value is valid and false otherwise. The second element is a '
                    + 'function that should return the error message.');
            }
        }
    }
    checkType(path, type) {
        // Make sure type has been set
        if (type == null) {
            throw new Error(path + ' is missing a type field.');
        }
        // Check if primitive type used
        const primitives = ['string', 'number', 'boolean', 'date', 'object', 'any'];
        if (primitives.includes(type)) {
            if (type === 'object') {
                this.noStripKeys.add(path);
            }
            // Check if type is array
        }
        else if (Array.isArray(type)) {
            // Make sure the array is the correct length
            if (type.length !== 1) {
                throw new Error(path + '.type is set to an array, ' +
                    'this array should have exactly one element either ' +
                    primitives + ' or an object');
            }
            // If element is an object make recursive call to validate it
            const element = type[0];
            if (typeof element === 'object' && !Array.isArray(element)) {
                this.parseSchema(path, element);
                // If element is not a primitive throw error
            }
            else if (primitives.includes(element)) {
                if (element === 'object') {
                    this.noStripKeys.add(path);
                }
            }
            else {
                throw new Error(path + '.type is set to an array, ' +
                    ' and has only one element however this element should either be ' +
                    primitives + ' or an object');
            }
            // Check if type is an object, if it is make recursive call to validate it
        }
        else if (typeof type === 'object') {
            this.parseSchema(path, type);
            // else throw error
        }
        else {
            throw new Error(path + '.type must have either ' + primitives + ', '
                + 'an array or an object as it\'s value');
        }
    }
    validateStandardSchema(field, fieldName, path) {
        const type = typeof field;
        const isValid = type === 'undefined' || type === 'boolean' || type === 'function' ||
            this.validateArray(field, 2, ['boolean', 'function'], ['string', 'function']);
        if (!isValid) {
            throw new Error(SchemaParser.standardSchemaError(path, fieldName));
        }
    }
    validateArray(array, length, el1Types, el2Types) {
        // Is array of length 2
        return Array.isArray(array) && array.length === length &&
            // Check first element is one of the allowed types
            el1Types.includes(typeof array[0]) &&
            // Check second element is one of the allowed types
            el2Types.includes(typeof array[1]);
    }
}
exports.default = SchemaParser;
