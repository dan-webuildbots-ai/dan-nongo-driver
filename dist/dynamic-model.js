"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const model_1 = require("./model");
class DynamicModel extends model_1.default {
    constructor(nongo, obj) {
        super(nongo, obj);
        this.dynamic = true;
    }
}
exports.default = DynamicModel;
