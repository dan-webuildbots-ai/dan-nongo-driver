import MongoIndex from './mongo-index';
/**
 * Collection used to maintain a set of {@link MongoIndex}s. {@link MongoIndex}s are concidered the same if they have
 * a matching {@code name}.
 */
export default class MongoIndexSet {
    private indexMap;
    add(mIndex: MongoIndex): void;
    get(mIndex: MongoIndex): MongoIndex;
    has(mIndex: MongoIndex): void;
    values(): IterableIterator<MongoIndex>;
    remove(mIndex: any): boolean;
}
