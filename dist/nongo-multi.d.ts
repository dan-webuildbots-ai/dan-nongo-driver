import NongoParams from './nongo-params';
/**
 * Used to efficiently set up and maintain multiple nongo connections that use the same models
 */
export default class NongoMulti {
    private paramsArray;
    private modelClasses;
    private nongoMap;
    constructor(paramsArray: NongoParams[], modelClasses: any[]);
    connect(): Promise<NongoMulti>;
    getNongo(id: string): any;
    private checkHasId;
}
