const gulp = require('gulp'),
  ts = require('gulp-typescript').createProject('tsconfig.json'),
  merge = require('merge-stream'),
  clean = require('gulp-clean'),
  mocha = require('gulp-mocha'),
  tslint = require('gulp-tslint'),
  stylish = require('gulp-tslint-stylish'),
  shell = require('gulp-shell');

const srcFiles = 'src/**/*.ts';
const testFiles = 'test/**/*.ts';

gulp.task('default', ['clean', 'lint'], function () {
  return gulp.src(srcFiles)
      .pipe(ts())
      .pipe(gulp.dest('dist'));
});

gulp.task('lint', function () {
  return gulp.src([srcFiles, testFiles])
    .pipe(tslint())
    .pipe(tslint.report(stylish, {
      emitError: false,
      sort: true,
      bell: true,
      fullPath: true
    }));
});

gulp.task('fix', shell.task([
  'tslint src/**/*.ts --fix',
  'tslint test/**/*.ts --fix'
]))

gulp.task('test', ['compileTests'], function () {
  return gulp.src('testing/compiled/test/**/*.js', {read: false})
    .pipe(mocha({timeout: 5000}))
});

gulp.task('compileTests', ['copyTestSources'], function () {
  return gulp.src(['testing/sources/**/*.*'])
    .pipe(ts())
    .pipe(gulp.dest('testing/compiled/'));
});

gulp.task('copyTestSources', ['clean'], function () {
  return merge(

    gulp.src('src/*')
      .pipe(gulp.dest('testing/sources/src')),

    gulp.src('test/*')
      .pipe(gulp.dest('testing/sources/test'))

  )
});

gulp.task('clean', function () {
  return gulp.src(['testing/', 'js'], {read: false})
        .pipe(clean());
});
