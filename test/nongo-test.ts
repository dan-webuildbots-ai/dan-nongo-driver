import {assert} from 'chai';
import * as clone from 'clone-deep';
import * as fs from 'fs';
import {MongoClient} from 'mongodb';
import * as rimraf from 'rimraf';
import GridFs from '../src/grid-fs';
import logger from '../src/logger';
import Nongo from '../src/nongo';
import DummyModel from './dummy-model';
import DummyModelChanged from './dummy-model-changed';

// Catch unhanded promise rejections and log them
process.on('unhandledRejection', (reason, p) => {
  logger.error(reason);
});

const url = 'mongodb://localhost:27017/nodeunittest';

const params = {
  host: 'localhost',
  port: 27017,
  db: 'nodeunittest',
};

async function drop() {
  const db = await MongoClient.connect(url);
  await db.dropDatabase();
  await db.close();
}

async function find() {
  const db = await MongoClient.connect(url);
  return await db.collection('DummyModel').find({}).toArray();
}

const models = [DummyModel, GridFs];
const pets = [
  {
    species: 'dog',
  },
  {
    species: 'dog',
  },
];
const job = {
  role: 'dogsbody',
  at: 'bing',
};

const obj1: any = {
  name: 'obj1',
  age: 123,
  created: new Date('2014-01-22T14:56:59.301Z'),
  dontStripChildren: {
    a: 1,
    b: 2,
  },
  arrayOfObject: [{}, {}],
  pets,
  job,
};

const obj2: any = {
  name: 'obj2',
  age: 321,
  pets,
  job,
};

describe('Testing DB operations...', () => {

  before(async () => await drop());
  afterEach(async () => await drop());

  /*
   * Tests the saving and updating of single objects
   */
  it('Test save', async () => {

    const mutableObj: any = {
      name: 'mutable',
      age: 123456,
      pets,
      job,
    };

    const nongo = await new Nongo(params, models).connect();

    const model = await nongo.new.DummyModel(mutableObj).save();

    // Check the returned model has an _id
    assert(model.obj._id, '_id has not been set on the saved object');

    // Copy the _id to obj1 and check that the objects are the same
    mutableObj._id = model.obj._id;
    assert.deepEqual(mutableObj, model.obj);

    // Query the database and make sure that the stored doc is correct
    let docs = await find();
    assert.lengthOf(docs, 1);
    assert.deepEqual(docs[0], mutableObj);

    // Update name
    model.obj.name = 'new name';
    // This both otherKey fields should be removed as it is not in the schema
    model.obj.otherKey = 'otherKey';
    model.obj.pets = [
      {
        species: 'some species',
        otherKey: 'otherKey',
      },
      {
        species: 'another species',
      },
    ];
    const presave = model.obj;

    const updated = await model.save();

    // Check that {@code otherKey}s are still present on presave
    assert(presave.otherKey);
    assert(presave.pets[0].otherKey);

    // Then remove them
    delete presave.otherKey;
    delete presave.pets[0].otherKey;

    // Check the updated model is returned correctly
    assert.deepEqual(model.obj, updated.obj);

    // Query the database and make sure that the stored doc is correct
    docs = await find();
    assert.lengthOf(docs, 1);
    assert.deepEqual(docs[0], model.obj);

    // Save another object
    await nongo.new.DummyModel(obj2).save();
    docs = await find();
    assert.lengthOf(docs, 2);

    nongo.db.close();
  });

  /*
   * Tests finding objects. We can start using nongo save here as we have
   * confirmed that it has worked in the previous test
   */
  it('Test find', async () => {
    // Save the objects
    const nongo: any = await new Nongo(params, models).connect();
    await nongo.new.DummyModel(obj1).save();
    await nongo.new.DummyModel(obj2).save();

    // Find all, with options
    const options = {
      sort: {name: -1},
    };
    let docs = await nongo.DummyModel.find({}, options);
    assert.lengthOf(docs, 2);
    assert.equal(docs[0].obj.name, obj2.name);
    assert.equal(docs[1].obj.name, obj1.name);

    // Find with query
    docs = await nongo.DummyModel.find({name: 'obj1'});
    assert.lengthOf(docs, 1);
    assert.deepEqual(docs[0].obj, obj1);

    nongo.db.close();
  });

  /*
   * Tests finding objects. We can start using nongo save here as we have
   * confirmed that it has worked in the previous test
   */
  it('Test findOne', async () => {
    // Save the objects
    const nongo: any = await new Nongo(params, models).connect();
    await nongo.new.DummyModel(obj1).save();
    await nongo.new.DummyModel(obj2).save();

    // Find one doc
    let doc = await nongo.DummyModel.findOne({name: 'obj1'});

    // Check the correct doc was returned
    assert.equal(doc.obj.name, 'obj1');
    assert.equal(doc.obj.age, 123);

    // Look for a doc that isn't there
    doc = await nongo.DummyModel.findOne({name: 'no objs with this name'});
    assert.isNull(doc);

    // Finish the test
    nongo.db.close();
  });

  /*
   * Tests validating objects.
   */
  it('Test validation', async () => {

    const validObj = {
      name: 'obj1',
      age: 123,
      created: new Date(),
      pets: [
        {
          species: 'dog',
          age: 7,
          likes: {
            food: [
              'dog food',
              'human food',
            ],
            drink: [
              'water',
            ],
          },
        },
        {
          species: 'cat',
          age: 9,
          likes: {
            food: [
              'cat food',
              'human food',
            ],
            drink: [
              'water',
              'milk',
            ],
          },
        },
      ],
      job: {
        role: 'CEO',
        at: 'Pets R\' Us',
      },
      notEmptyFields: {
        aString: 'this is a string',
        anArray: ['1', '2', '3'],
      },
    };

    const invalidObj = {
      name: 'invalid name',
      created: 'should be a date',
      pets: [
        {
          species: 7,
          age: 7,
          likes: {
            food: [
              'dog food',
              'human food',
            ],
            drink: [123],
          },
        },
        {
          species: 'cat',
          age: 9,
          likes: {
            food: [
              123,
              123,
            ],
            drink: 'I should be an array',
          },
        },
      ],
      job: {
        role: 123,
      },
      location: 'should be an object',
      notEmptyFields: {
        aString: null,
        anArray: [],
      },
    };

    const nongo: any = new Nongo(params, models);
    nongo.noIndex = true;
    await nongo.connect();

    // Check validation fails for invalidObj
    let errors = await nongo.new.DummyModel(invalidObj).validate();
    const expected = ['"created" must be of type "date".',
      '"invalid name" is not a valid name',
      'DummyModels must have an age',
      '"pets.species" must be of type "string".',
      '"pets.likes.food" must be an array of "string" elements.',
      '"pets.likes.drink" must be an array of "string" elements.',
      '"job.role" must be of type "string".',
      'job.at must be set if job.role is',
      'notEmptyFields.aString must be a string with at least one character',
      '"notEmptyFields.anArray" cannot be empty',
      'pets.likes.drink must be an array',
      'location must be an object',
    ];
    assert.deepEqual(errors.sort(), expected.sort());

    // Check validation passes for valid object
    let validModel = nongo.new.DummyModel(clone(validObj));
    errors = await validModel.validate();
    assert.isNull(errors);

    // Check validation doesn't fail on unique index when it's the same model.
    // note when save returns the _id is set on valid model.
    validModel = await validModel.save();
    assert.isNull(await validModel.validate());

    // Check validation fails on unique index when it's the not same model (this one has no _id)
    errors = await nongo.new.DummyModel(validObj).validate();
    assert.deepEqual(errors, ['name has a unique index and "obj1" already exists in it']);

    nongo.db.close();

  });

  /*
   * Tests finding objects. We can start using nongo find and save here as they
   * have been tested above
   */
  it('Test delete', async () => {

    const nongo: any = await new Nongo(params, models).connect();
    const model1 = await nongo.new.DummyModel(obj1).save();
    await nongo.new.DummyModel(obj2).save();

    // Del obj 1 (using toHexString to keep test that prepareQuery works)
    const res = await nongo.DummyModel.remove({_id: model1.obj._id.toHexString()});
    // Check a model was deleted
    assert.deepEqual(res, {n: 1, ok: 1});

    const docs = await nongo.DummyModel.find();

    // Make sure the correct doc was deleted
    const model = docs[0];
    assert.lengthOf(docs, 1);
    assert.equal(model.obj.name, obj2.name);
    assert.equal(model.obj.age, obj2.age);

    // Finish the test
    nongo.db.close();
  });

  it('Test indexes have been applied', async () => {
    const db = await MongoClient.connect(url);

    let expected: any = [
      {
        v: 2,
        key: {
          _id: 1,
        },
        name: '_id_',
        ns: 'nodeunittest.DummyModel',
      },
      {
        v: 2,
        unique: true,
        key: {
          name: 1,
        },
        name: 'name_1',
        ns: 'nodeunittest.DummyModel',
      },
      {
        v: 2,
        key: {
          'pets.likes.drink': 1,
        },
        name: 'pets.likes.drink_1',
        ns: 'nodeunittest.DummyModel',
      },
      {
        v: 2,
        key: {
          'job.role': 1,
        },
        name: 'job.role_1',
        ns: 'nodeunittest.DummyModel',
      },
    ];

    let nongo = await new Nongo(params, [DummyModel]).connect();
    let indexes = await db.collection('DummyModel').indexes();
    assert.deepEqual(indexes, expected);
    nongo.db.close();

    expected = [
      {
        v: 2,
        key: {
          _id: 1,
        },
        name: '_id_',
        ns: 'nodeunittest.DummyModel',
      },
      {
        v: 2,
        key: {
          name: 1,
        },
        name: 'name_1',
        ns: 'nodeunittest.DummyModel',
      },
      {
        v: 2,
        key: {
          'pets.likes.drink': 1,
        },
        name: 'pets.likes.drink_1',
        ns: 'nodeunittest.DummyModel',
      },
    ];

    nongo = await new Nongo(params, [DummyModelChanged]).connect();
    indexes = await db.collection('DummyModel').indexes();
    assert.deepEqual(indexes, expected);
    nongo.db.close();

    db.close();
  });

  it('Test aggregation', async () => {
    const nongo: any = await new Nongo(params, models).connect();
    await nongo.new.DummyModel(obj1).save();
    await nongo.new.DummyModel(obj2).save();

    const pipeline = [
      {
        $group: {
          _id: '$name',
          count: {$sum: 1},
        },
      },
    ];

    const options = {
      sort: {name: -1},
    };

    const expected = [
      {
        _id: 'obj2',
        count: 1,
      },
      {
        _id: 'obj1',
        count: 1,
      },
    ];

    // Aggreate and check result is correct
    const results = await nongo.DummyModel.aggregate(pipeline, options);
    assert.lengthOf(results, 2);
    assert.deepEqual(results, expected);

    nongo.db.close();
  });

  it('Test update', async () => {
    const nongo: any = await new Nongo(params, models).connect();
    await nongo.new.DummyModel(obj1).save();
    await nongo.new.DummyModel(obj2).save();

    const newAge = 999999999;
    const result = await nongo.DummyModel
      .update({}, {$set: {age: newAge}});
    assert.deepEqual(result, {n: 2, nModified: 2, ok: 1});

    const doc = await nongo.DummyModel.findOne();
    assert.equal(doc.obj.age, newAge);

    nongo.db.close();
  });

  /*
   * Tests finding objects. We can start using nongo save here as we have
   * confirmed that it has worked in the previous test
   */
  it('Test distinct', async () => {
    // Save the objects
    const nongo: any = await new Nongo(params, models).connect();
    await nongo.new.DummyModel(obj1).save();
    await nongo.new.DummyModel(obj2).save();

    let docs = await nongo.DummyModel.distinct('name');
    assert.lengthOf(docs, 2);
    docs = docs.sort();
    assert.equal(docs[0], obj1.name);
    assert.equal(docs[1], obj2.name);

    // Finish the test
    nongo.db.close();
  });

  it('Test count', async () => {
    // Make sure count is 0
    const nongo: any = await new Nongo(params, models).connect();
    assert.equal(await nongo.DummyModel.count(), 0);

    // Save the objects
    await nongo.new.DummyModel(obj1).save();
    await nongo.new.DummyModel(obj2).save();

    // Test count
    assert.equal(await nongo.DummyModel.count(), 2);
    assert.equal(await nongo.DummyModel.count({name: obj1.name}), 1);

    // Finish the test
    nongo.db.close();
  });

  it('Test dropping database', async () => {
    // Insert and confirm it worked
    const nongo: any = await new Nongo(params, models).connect();
    await nongo.new.DummyModel(obj1).save();
    assert.isNotNull(await nongo.DummyModel.findOne());

    // Drop and confirm it worked
    await nongo.dropDatabase();
    assert.isNull(await nongo.DummyModel.findOne());

    // Finish the test
    nongo.db.close();
  });

  it('Test creating collection dump', async () => {
    // Insert and confirm it worked
    const nongo: any = await new Nongo(params, models).connect();
    await nongo.new.DummyModel(obj1).save();
    assert.isNotNull(await nongo.DummyModel.findOne());

    const dir = './testing/temp-dump';

    // Make sure {@code dir} doesn't exist
    assert(!fs.existsSync(dir));

    await nongo.DummyModel.dumpCollection(dir, true);

    // Check the dir has been populated by the dump
    assert(fs.readdirSync(dir).length);

    // Clean up {@code dir}
    rimraf(dir, () => logger.info('Cleaned up ' + dir));

    // Finish the test
    nongo.db.close();
  });

  it('Test gridfs', async () => {
    const nongo: any = await new Nongo(params, models).connect();
    const gfs = nongo.gfs;

    // Assert there and no files in gridfs
    assert.equal(await nongo.GridFs.count(), 0);

    // Write a file to gridfs
    const path = './test-res/tiny.png';
    const fileStream = fs.createReadStream(path);
    // This is an instance of {@link GridFs}
    const options = {
      filename: path,
      metadata: {
        key: 'value',
      },
    };
    const returnedFromWrite = await gfs.write(fileStream, options);
    assert.equal(returnedFromWrite.obj.filename, path);
    assert.deepEqual(returnedFromWrite.obj.metadata, options.metadata);

    // Slightly modify returnedFromWrite to that we can use deepEquals
    const undefinedToNull = (array) => array || null;
    returnedFromWrite.obj.aliases = undefinedToNull(returnedFromWrite.obj.aliases);
    returnedFromWrite.obj.metadata = undefinedToNull(returnedFromWrite.obj.metadata);

    // Check the meta data was stored correctly in the GridFs collection.
    assert.equal(await nongo.GridFs.count(), 1);
    const gfsFile = await nongo.GridFs.findOne();
    assert.deepEqual(gfsFile.obj, returnedFromWrite.obj);

    // Check when the file is read back it is the same
    gfsFile.getReadStream();
    // TODO need assertions that make sure that this file is the same

    // TODO Test fail to write
    // TODO test fail to read

    // Finish the test
    nongo.db.close();
  });

});
