import {assert} from 'chai';
import {MongoClient} from 'mongodb';
import logger from '../src/logger';
import NongoMulti from '../src/nongo-multi';
import DummyModel from './dummy-model';

// Catch unhanded promise rejections and log them
process.on('unhandledRejection', (reason, p) => {
  logger.error(reason);
});

const dbNames = ['nodeunittest-nongo-multi-1', 'nodeunittest-nongo-multi-2', 'nodeunittest-nongo-multi-3'];

async function drop() {
  for (const dbName of dbNames) {
    const db = await MongoClient.connect('mongodb://localhost:27017/' + dbName);
    await db.dropDatabase();
    await db.close();
  }
}

const models = [DummyModel];

describe('Testing NongoMulti...', () => {

  before(async () => await drop());
  afterEach(async () => await drop());

  /*
   * Tests finding objects. We can start using nongo save here as we have
   * confirmed that it has worked in the previous test
   */
  it('Test connect', async () => {
    const nameToId = (dbName) => dbName + '-id';

    // Create an array of params one for each db name
    const paramsArray = dbNames.map((dbName) => ({
      id: nameToId(dbName),
      host: 'localhost',
      port: 27017,
      db: dbName,
    }));

    // Connect a nongo multi instance
    const nongoMulti = await new NongoMulti(paramsArray, models).connect();

    // Test the connections are configured correctly
    for (const dbName of dbNames) {
      const nongo = nongoMulti.getNongo(nameToId(dbName));
      assert.equal(dbName, nongo.dbName);
    }

  });

});
