import Model from '../src/model';
import Nongo from '../src/nongo';

export default class DummyModelChanged extends Model {

  constructor(nogo: Nongo, obj: any) {
    super(nogo, obj);
  }

  public defineSchema(): any {
    return {

      name: {
        type: 'string',
        required: true,
        // This used to have a unquie index now it is a normal one
        indexed: true,
        validate: [
          (value, obj) => value !== 'invalid name',
          (value, obj) => '"' + value + '" is not a valid name',
        ],
      },

      pets: {
        required: [true, 'pets array must be present'],
        validate: [
          (value, obj) => value.length === 2,
          (value, obj) => '***path*** should only have 2 elements',
        ],
        type: [
          {

            species: {
              type: 'string',
            },

            likes : {

              type: {

                food: {
                  type: ['string'],
                },

                drink: {
                  // This index is the same
                  indexed: true,
                  type: ['string'],
                },

              },

            },

          },
        ],
      },

    };

    // All the other fields are gone so their indexes should be dropped too
  }

  protected defineCollection(): string {
    return 'DummyModel';
  }

}
