import { assert } from 'chai';
import SchemaParser from '../src/schema-parser';

function validSchema() {
  return {
    name: {
      type: 'string',
      unique: [true, (val, obj) => '***path*** has a unique index and '
        + val + ' already exist in it'],
      required: [true, 'Must have a name'],
    },

    address: {

      required: [
        (val, obj) => true,
        'Must have a name',
      ],

      type: {
        address1: {
          type: 'string',
        },
      },

    },

    phone: {
      indexed: true,
      type: ['number'],
      validate: [
        (val, obj) => true,
        (val, obj) => 'This is not a valid phone number',
      ],
    },

    likes: {
      type: [
        {
          name: {
            type: 'string',
            notEmpty: true,
          },
        },
      ],
      notEmpty: [true, 'likes should not be empty'],
    },

    something: {
      type: 'any',
      unique: true,
    },

  };
}

const collection = 'testing';

describe('Testing schema validation...', () => {

  it('Test valid schema', (done) => {
    const parser = new SchemaParser(validSchema(), collection);
    done();
  });

  it('Test invalid required field', (done) => {
    const error = SchemaParser.standardSchemaError('name', 'required');

    let schema = validSchema();
    schema.name.required[0] = 'should be boolean or function';
    assert.throws(() => new SchemaParser(schema, collection), error);

    schema = validSchema();
    // Should be a string or a function
    schema.name.required[1] = true;
    assert.throws(() => new SchemaParser(schema, collection), error);

    done();
  });

  it('Test invalid type field', (done) => {
    let schema = validSchema();
    delete schema.name.type;
    assert.throws(() => new SchemaParser(schema, collection), 'name is missing a type field.');

    schema = validSchema();
    schema.name.type = 'not a valid type';
    assert.throws(() => new SchemaParser(schema, collection),
      'name.type must have either string,number,boolean,date,object,any, an array or an object as it\'s value');

    done();
  });

  it('Test invalid validate field', (done) => {
    const error = SchemaParser.standardSchemaError('phone', 'validate');

    let schema: any = validSchema();
    schema.phone.validate = 'this should be an array with two functions.';
    assert.throws(() => new SchemaParser(schema, collection), error);

    schema = validSchema();
    schema.phone.validate[0] = 'This should be a function';
    assert.throws(() => new SchemaParser(schema, collection), error);

    schema = validSchema();
    schema.phone.validate.push(() => 'This is one function too many');
    assert.throws(() => new SchemaParser(schema, collection), error);

    done();
  });

  it('Test invalid indexed field', (done) => {
    const schema: any = validSchema();
    schema.phone.indexed = 'should be a boolean';
    assert.throws(() => new SchemaParser(schema, collection),
      'If set, phone.indexed must be a boolean');

    done();
  });

  it('Test invalid unique field', (done) => {
    const schema: any = validSchema();
    schema.name.unique = 'should be an array or boolean';
    assert.throws(() => new SchemaParser(schema, collection), SchemaParser.standardSchemaError('name', 'unique'));

    done();
  });

  it('Test invalid notEmpty field', (done) => {
    let schema: any = validSchema();
    schema.name.notEmpty = 'should be an array';
    assert.throws(() => new SchemaParser(schema, collection), SchemaParser.standardSchemaError('name', 'notEmpty'));

    schema = validSchema();
    schema.other = {
      type: 'number',
      notEmpty: [true, 'should fail as type is number'],
    };
    assert.throws(() => new SchemaParser(schema, collection), SchemaParser.notEmptyTypeError('other'));

    done();
  });

  it('Test invalid schema fields', (done) => {
    const schema: any = validSchema();
    schema.phone.badField = 'this is not a valid field';
    assert.throws(() => new SchemaParser(schema, collection), 'phone.badField is not a vaid'
      + ' schema key, should badField be within a "type" object / the root object?');

    done();
  });

  it('Test validKeys', (done) => {
    const expected = new Set([
      '_id',
      'name',
      'address',
      'address.address1',
      'phone',
      'likes',
      'likes.name',
      'something',
    ]);

    const schema: any = validSchema();
    assert.deepEqual(new SchemaParser(schema, collection).validKeys, expected);

    done();
  });

});
